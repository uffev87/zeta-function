#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  2 14:16:36 2023

@author: ulf
"""

import numpy as np
import matplotlib.pyplot as plt

pi = np.pi
inf = float('inf')

# factorial of integer values

def factorial(n):

    for i in range(n + 1):

        if i == 0:
            y = 1
        else:
            y *= i

    return y

# double factorial of integer values

def fact_double(n):

    if n == -1 or n == 0:
        y = 1

    for i in range(n, 0, -2):

        if i == n:
            y = i
        else:
            y *= i

    return y

# gamma(z) = int_0^înf t^(z - 1)exp(-t)dt = (z - 1)!
# approximated by Stirling's formula below

def gamma(z):

    if z.real <= 0 and z == int(z.real):

        y = inf

    elif z.real < 0.5:

        y = pi/(np.sin(pi * z) * gamma(1 - z))

    elif 0.5 <= z.real < 3.5:

        y = gamma(z + 1)/z

    else:

        z -= 1

        m = 10 # values of a_n=(2n+1)!!b_(2n+1) decrease until m=10

        b = np.empty(2 * m + 2)
        b[0] = b[1] = 1
        for n in range(2, 2 * m + 2):
            b[n] = (b[n - 1] - \
                    sum(k * b[k] * b[n + 1 - k] for k in range(2, n)))/(n + 1)

        a = np.array([fact_double(2 * n + 1) * b[2 * n + 1] \
                      for n in range(m + 1)])

        y = np.exp(-z) * z**z * np.sqrt(2 * pi * z) * \
            sum(a[n]/z**n for n in range(m + 1))

    return y

#eta(s) = sum_(n=1)^inf (-1)**(n - 1)/n^s, approximated by Borwein's method

def eta(s, tol):

    if s == 0:

        y = 0.5

    elif s.real < 0.5:

        y = 2 * (1 - 2**(s - 1))/(1 - 2**s) * pi**(s - 1) * s * \
            np.sin(0.5 * pi * s) * gamma(-s) * eta(1 - s, tol)

    else:

        t = abs(s.imag)

        ln_num = np.log(3/tol * (1 + 2 * t))
        ln_den = np.log(3 + np.sqrt(8))

        n = (0.5 * pi * t + ln_num)/ln_den # iterations to reach error bound
        n = int(n) + 1

        d = np.empty(n + 1)
        for k in range(n + 1):
            d[k] = n * sum(factorial(n + l - 1) * 4**l/ \
                           (factorial(n - l) * factorial(2 * l)) \
                               for l in range(k + 1))

        y = -sum((1 - 2 * (k%2)) * (d[k] - d[n])/(k + 1)**s \
                 for k in range(n))/d[n]

    return y

# approximating the zeta function, by,
# zeta(sigma + it) = eta(sigma + it)/(1 - 2**(1 - s))

def zeta(s, tol):

    y = eta(s, tol)/(1 - 2**(1 - s))

    return y

# creates equidistant values for [a, b], used for plotting

def linspace(a, b, N):

    y = np.empty(N)
    for i in range(N):
        y[i] = ((N - 1 - i) * a + i * b)/(N - 1)

    return y

# plots the zeta function, zeta(sigma + it),
# for a given sigma and several (N) values of t.

def plot(sigma, t_min, t_max, tol, N):

    y = []

    t = linspace(t_min, t_max, N)

    for i in range(N):

        y.append(zeta(complex(sigma, t[i]), tol))

    y = np.array(y)

    fig1, ax1 = plt.subplots()

    ax1.set_title('Polar graph of $\zeta({} + it)$'.format(sigma))
    ax1.plot(y.real[N - N//2:], y.imag[N - N//2:], color = 'red', label = 'Re($\zeta({} + it)$)'.format(sigma))
    ax1.set_xlabel('Re($\zeta({} + it)$)'.format(sigma))
    ax1.set_ylabel('Im($\zeta({} + it)$)'.format(sigma))
    ax1.grid(which = 'major', linewidth = 1.2)

    fig2, ax2 = plt.subplots()

    ax2.set_title('Real and imaginary parts of $\zeta({} + it)$'.format(sigma))
    ax2.plot(t, y.real, color = 'red', label = 'Re($\zeta({} + it)$)'.format(sigma))
    ax2.plot(t, y.imag, color = 'blue', label = 'Im($\zeta({} + it)$)'.format(sigma))
    ax2.legend(loc = 'best')
    ax2.grid(which = 'major', linewidth = 1.2)
    ax2.grid(which = 'minor', linestyle = 'dashed', linewidth = 0.5)
    ax2.minorticks_on()
